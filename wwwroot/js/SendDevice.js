﻿document.addEventListener("DOMContentLoaded", () => {
  let enviar = document.querySelector("#enviar");
  enviar.addEventListener("click", () => {
    let titulo = document.querySelector("#Title");
    let mensaje = document.querySelector("#Message");
    let play = document.querySelector("#Payload");
    let payloadObject = {
      title: titulo.value,
      message: mensaje.value,
    };
    play.value = JSON.stringify(payloadObject);
  });
});
