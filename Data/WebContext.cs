
using Microsoft.EntityFrameworkCore;

namespace Web.Models
{
    public class WebContext : DbContext
    {
        public WebContext(DbContextOptions<WebContext> options)
            : base(options)
        {
        }

        public DbSet<Web.Models.Devices> Devices { get; set; }
    }
}